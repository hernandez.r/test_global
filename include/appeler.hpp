#ifndef INCLUDE_APPELER_HPP_
#define INCLUDE_APPELER_HPP_

#include <string>
#include <iostream>

class TestR
{
public:
  TestR();
  TestR(std::string reponse,std::string personne);
  friend std::ostream& operator<<(std::ostream& os, const TestR& dt);

  void appeler(TestR &cible);
  void reponse();

private:
  std::string rep_;
  std::string pers;
};


#endif
