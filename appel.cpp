#include <iostream>
#include "appeler.hpp"

int main(void)
{
  TestR romain("salut","romain"),victor("bonjour","victor");
  romain.appeler(victor);
  victor.appeler(romain);
  return 0;

}
