#include "appeler.hpp"

//using std::cout;
//using std::endl;

TestR::TestR() :
        rep_("")
{
}

TestR::TestR(std::string reponse,std::string personne) :
        rep_(reponse),pers(personne)
{
}

std::ostream& operator<<(std::ostream& os, const TestR& dt)
{
    os << dt.pers;
    return os;
}

void TestR::appeler(TestR &cible)
{
  std::cout <<*this<< " appelle " << cible << std::endl;
  cible.reponse();
}

void TestR::reponse()
{
  std::cout << rep_ << std::endl;
}


